#ifndef ARBOL_H
#define ARBOL_H
#include <iostream>
#include <stdlib.h>
using namespace std;

/**************************Árbol Genérica*********************************/
class ArbolGenerico{
	
	
	
class NodoArbol{
public:

NodoArbol *izquierda;
NodoArbol *derecha;
int elemento;

insertar(){return elemento;}

NodoArbol(int x){
elemento = x;
izquierda = NULL;
derecha = NULL;
}
};

NodoArbol *referencia, *raiz;

	void IzqSig(NodoArbol*hoja){this->izquierda=hoja;}
	void DerSig(NodoArbol*hoja){this->derecha=hoja;}
	NodoArbol*IzqSig(){return this->izquierda;}
	NodoArbol*DerSig(){return this->derecha;}

public:
/*-----------------------------------------------------------------------------------------------------------*/
	
//*********************** DECLARACION DE FUNCIONES***********************
bool arbolVacio();
void meter(int elemento);
// void raiz();
// void Preorden(void (*func)(int&) , Nodo *nodo=NULL, bool r=true);
/*void Inorden(NodoArbol *raiz);
void Postorden(NodoArbol *raiz);
*/
void Preorden(int);

	
// *********************** CONSTRUCTOR ***********************
ArbolGenerico(){
	raiz = NULL;
}

};


/*---------------------------------------IMPLEMENTACIÓN DE FUNCIONES---------------------------------------------*/



//*************FUNCION METER DATO NUEVO********************
void ArbolGenerico::meter(int dato)
{
	int vueltas=0;
	NodoArbol *cargar;
	system("clear");
	if(arbolVacio()){
		cout<<"*******************************************\n";
		cout<<"*           El nodo es una raíz           *\n";
		cout<<"*******************************************\n\n";
		
		raiz = new NodoArbol(dato);
	}
	if(!arbolVacio()){
		referencia = raiz;
		//Si el elemento introducido es mayor que el elemento de la referencia entonces..
		while((referencia != NULL)){
		
		cargar = referencia;
			if(dato > (referencia->elemento)){
				cout<<"Se insertarán por la derecha\n";
				referencia = referencia->derecha;
			}
			else if(dato < (referencia->elemento)){ //Sino, si el elemento introducido es menor que el elemento de la referencia entonces..
				cout<<"Se insertarán por la izquierda\n";
				referencia = referencia->izquierda;
			}else return;
		}
		
		//Una vez que se recorrio el arbol, hay que insertarlos:
		//Si el elemento introducido es mayor que el elemento de la referencia entonces..
		if(dato > cargar->elemento){
			cargar->derecha = new NodoArbol(dato);
		}
		else if(dato < cargar->elemento){
			cargar->izquierda = new NodoArbol(dato);
		}
		
		cout<<"El nodo no es una raiz\nEl elemento es: "<<dato;
		cout<<"\nLa referencia ahora es: "<<cargar->elemento <<"\nY la raiz es: "<<raiz->elemento<<"\n\n";
	}
}//*********************************************************


// void ArbolGenerico::raiz(){
// 	return raiz;
// }



//*************FUNCION ARBOL VACIO********************
bool ArbolGenerico::arbolVacio()
{
return raiz == NULL;
}
//****************************************************

//*************IMPRIMIR********************
void Imprimir(int &dat)
{
   cout << dat << ",";
}



//***********************RECORRIDO PREORDEN*********************
void ArbolGenerico::Preorden(NodoArbol*raiz){
	NodoArbol *temp=raiz;
	if(temp){
		if(temp->IzqSig()||temp->DerSig())
			cout<<temp->insertar()<<",";//Raiz
		if(temp->IzqSig())Preorden(temp->IzqSig());//Izquierda
		if(temp->DerSig())Preorden(temp->DerSig());//Derecha
		if(!temp->IzqSig()&&!temp->DerSig())
			cout<<temp->insertar()<<",";
	}
}

/*
void ArbolGenerico::Inorden(NodoArbol * nodo)
{
	if(nodo != NULL){return;}
	if(nodo->izquierdo) Inorden(nodo->izquierdo);
	cout<<this->nodo->dato<<" ";
	if(nodo->derecho) Inorden(nodo->derecho);
	
}

void ArbolGenerico::Postorden(NodoArbol * nodo)
{
	if(nodo != NULL){return;}
	if(nodo->izquierdo) Postorden(nodo->izquierdo);
	if(nodo->derecho) Postorden(nodo->derecho);
	cout<<this->nodo->dato<<" ";
	
}
*/
#endif