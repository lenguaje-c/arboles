#ifndef ARBOL_H
#define ARBOL_H
template<class t>class nodo{
	t dato;
	nodo<t>*izq,*der;
	public:
	nodo(){izq=der=NULL;}
	t&insertar(){return dato;}
	nodo<t>*IzqSig(){return this->izq;}
	nodo<t>*DerSig(){return this->der;}
	void IzqSig(nodo<t>*hoja){this->izq=hoja;}
	void DerSig(nodo<t>*hoja){this->der=hoja;}
};



template<class t>class arbol{
	nodo<t>*raiz;
	void Avanzar(nodo<t>*,nodo<t>*);
	public:
	arbol(){raiz=NULL;}
	void Insertar_d(t);
	nodo<t>*Raiz(){return raiz;}
	void Preorden(nodo<t>*);
	void Inorden(nodo<t>*);
	void Posorden(nodo<t>*);
};
template<class t> void arbol<t>::Insertar_d(t dato){
	nodo<t>*hoja=new nodo<t>;
	if (hoja) hoja->insertar()=dato;
	if (!raiz) { this->raiz=hoja;
	}else { Avanzar(raiz,hoja); }
}
template<class t> void arbol<t>::Avanzar(nodo<t>*padre,nodo<t>*hoja){
	nodo<t>*temp=padre;
	if(temp){
		if(temp->insertar() > hoja->insertar()){
			if(temp->IzqSig())
				Avanzar(temp->IzqSig(),hoja);
			else
				temp->IzqSig(hoja);
		}else{
			if(temp->DerSig())
			Avanzar(temp->DerSig(),hoja);
			else
				temp->DerSig(hoja);
		}
	}
}
template<class t>void arbol<t>::Preorden(nodo<t>*raiz){
	nodo<t>*temp=raiz;
	if(temp){
		if(temp->IzqSig()||temp->DerSig())
			cout<<temp->insertar()<<",";//Raiz
		if(temp->IzqSig())Preorden(temp->IzqSig());//Izquierda
		if(temp->DerSig())Preorden(temp->DerSig());//Derecha
		if(!temp->IzqSig()&&!temp->DerSig())
			cout<<temp->insertar()<<",";
	}
}
template<class t>void arbol<t>::Inorden(nodo<t>*raiz){
	nodo<t>*temp=raiz;
	if(temp){
		if(temp->IzqSig())Inorden(temp->IzqSig());//Izquierda
		if(temp->DerSig()){
			cout<<temp->insertar()<<",";//Raiz
			Inorden(temp->DerSig());//Derecha
		}else cout<<temp->insertar()<<",";
	}
}
template<class t>void arbol<t>::Posorden(nodo<t>*raiz){
	nodo<t>*temp=raiz;
	if(temp){
		if(temp->IzqSig())Posorden(temp->IzqSig());//Izquierda
		if(temp->DerSig())Posorden(temp->DerSig());//Derecha
		cout<<temp->insertar()<<",";//Raiz
	}
}
#endif
